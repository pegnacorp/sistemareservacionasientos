
package Gestores.cliente;

import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import modelo.Mensaje;

/**
 *
 * @author Arian Castillo
 */
public class GestorCambios {
    
    public GestorCambios(Socket cliente) throws IOException {
        this.cliente = cliente;
//        inicializarBuferMenjesEnviados();
        inicializarBuferMensajesRecibidos();
    }
    
//    public void enviarMensaje(Mensaje mensaje) {
//        GestorJSON gestorJson = new GestorJSON();
//        String mensajeEmpacado = gestorJson.generarJSON(mensaje);
//        escritura.println(mensajeEmpacado);
//    }
    
    public Mensaje recibirMensaje() throws IOException {
        String mensaje = lectura.readLine();
        GestorJSON gestorJson = new GestorJSON();
        Mensaje mensajeDesempaquetado = gestorJson.generarMensaje(mensaje);
        return mensajeDesempaquetado;
    }
    
    private Socket cliente;
    private BufferedReader lectura;
//    private PrintWriter escritura;
    
    private void inicializarBuferMensajesRecibidos() throws IOException {
        lectura = new BufferedReader(new InputStreamReader(cliente.getInputStream()));
    }
    
//    private void inicializarBuferMenjesEnviados() throws IOException {
//        escritura = new PrintWriter(cliente.getOutputStream(), true);
//    }
}
