
package Gestores.cliente;

import java.io.IOException;
import javax.swing.JOptionPane;
import modelo.Mensaje;

/**
 *
 * @author Arian Castillo
 */
public class GestorSesion {
    
    public GestorSesion(GestorMensajeCliente mensajero){
        this.mensajero = mensajero;
    }
    
    public Mensaje iniciar(String ip, int puerto){
        GestorJSON gestorjs = new GestorJSON();
        Mensaje msj = new Mensaje(0);
        msj.setIp(ip);
        msj.setPuerto(puerto);
        String paquete = gestorjs.generarJSON(msj);
        mensajero.enviarMensaje(paquete);
        try {
            paquete = mensajero.recibirMensaje();
        } catch (IOException ex) {
            String nl = System.getProperty("line.separator");
            Object mensajeError = "Error al recibir respuesta del servidor." + 
                    nl + nl + "Detalles: " + nl + ex;
            JOptionPane.showMessageDialog(null, mensajeError, "ERROR DE CONEXION", JOptionPane.ERROR_MESSAGE);
        }
        msj = gestorjs.generarMensaje(paquete);
        return msj;
    }
    
    public void cerrar(){
        Mensaje msj = new Mensaje(1);
        GestorJSON gestorjs = new GestorJSON();
        String paquete = gestorjs.generarJSON(msj);
        mensajero.enviarMensaje(paquete);
    }
    
    public Mensaje recuperar(){
        Mensaje msj = new Mensaje(7);
        GestorJSON gestorjs = new GestorJSON();
        String paquete = gestorjs.generarJSON(msj);
        mensajero.enviarMensaje(paquete);
        try {
            String respuesta = mensajero.recibirMensaje();
            msj = gestorjs.generarMensaje(respuesta);
        } catch (IOException ex) {
            String nl = System.getProperty("line.separator");
            Object mensajeError = "Error al recibir respuesta del servidor." + 
                    nl + nl + "Detalles: " + nl + ex;
            JOptionPane.showMessageDialog(null, mensajeError, "ERROR DE CONEXION", JOptionPane.ERROR_MESSAGE);
        }
        return msj;
    }
    
    private final GestorMensajeCliente mensajero;
}
