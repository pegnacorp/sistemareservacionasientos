
package Gestores.cliente;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import modelo.Asiento;
import modelo.Mensaje;

/**
 *
 * @author Arian Castillo
 */
public class GestorReserva {
    
    public GestorReserva(GestorMensajeCliente mensajero){
        this.mensajero = mensajero;
    }
    
    public Mensaje reservar(Asiento asiento){
        Mensaje msj = new Mensaje(4);
        msj.setAsiento(asiento);
        gestorjs = new GestorJSON();
        String paquete = gestorjs.generarJSON(msj);
        mensajero.enviarMensaje(paquete);
        try {
            String respuesta = mensajero.recibirMensaje();
            msj = gestorjs.generarMensaje(respuesta);
        } catch (IOException ex) {
            String nl = System.getProperty("line.separator");
            Object mensajeError = "Error al recibir respuesta del servidor." + 
                    nl + nl + "Detalles: " + nl + ex;
            JOptionPane.showMessageDialog(null, mensajeError, "ERROR DE CONEXION", JOptionPane.ERROR_MESSAGE);
        }
        return msj;
    }
    
    public Mensaje cancelar(Asiento asiento){
        Mensaje msj = new Mensaje(5);
        msj.setAsiento(asiento);
        gestorjs = new GestorJSON();
        String paquete = gestorjs.generarJSON(msj);
        mensajero.enviarMensaje(paquete);
        try {
            String respuesta = mensajero.recibirMensaje();
            msj = gestorjs.generarMensaje(respuesta);
        } catch (IOException ex) {
            String nl = System.getProperty("line.separator");
            Object mensajeError = "Error al recibir respuesta del servidor." + 
                    nl + nl + "Detalles: " + nl + ex;
            JOptionPane.showMessageDialog(null, mensajeError, "ERROR DE CONEXION", JOptionPane.ERROR_MESSAGE);
        }
        return msj;
    }
    
    private final GestorMensajeCliente mensajero;
    private GestorJSON gestorjs;
}
