
package Gestores.cliente;

import com.google.gson.Gson;
import modelo.Mensaje;

/**
 *
 * @author Arian Castillo
 */
public class GestorJSON {
    public String generarJSON(Mensaje msj){
        final Gson gson = new Gson();
        final String representacionJSON = gson.toJson(msj);
        return representacionJSON;
    }
    public Mensaje generarMensaje(String json){
        final Gson gson = new Gson();
        final Mensaje msj = gson.fromJson(json, Mensaje.class);
        return msj;
    }
}
