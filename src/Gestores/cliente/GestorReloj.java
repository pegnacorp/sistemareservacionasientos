
package Gestores.cliente;

import static java.lang.Thread.sleep;
import javax.swing.JLabel;
import modelo.Reloj;

/**
 *
 * @author Arian Castillo
 */
public class GestorReloj implements Runnable{
    
    public GestorReloj(JLabel reloj){
        this.relojVista = reloj;
        this.reloj = new Reloj();
        hilo = new Thread(this);
    }
    
    public void iniciarReloj(){
        reloj.setSegundos(SEGUNDOSINICIO);
        reloj.setIniciado(true);
        hilo.start();
    }
    
    public void detenerReloj(){
        relojVista.setText("00:60");
        hilo.suspend();
    }
    
    public void reiniciarReloj(){
        reloj.setSegundos(SEGUNDOSINICIO);
        hilo.resume();
    }
    
    public boolean estaIniciado(){
        return reloj.getIniciado();
    }

    @Override
    public void run() {
        while(reloj.getIniciado()){
            try {
                sleep(1000);
            } catch (InterruptedException ex) {
                System.out.println("Error en el reloj");
            }
            if(reloj.getSegundos()!=0){
                if(reloj.getSegundos()<10)
                    relojVista.setText("00:0"+reloj.getSegundos());
                else
                    relojVista.setText("00:"+reloj.getSegundos());
                reloj.setSegundos(reloj.getSegundos()-1);
            }
            else{
                reiniciarReloj();
                detenerReloj();
            }
        }
    }
    
    private static final int SEGUNDOSINICIO = 60;
    private final JLabel relojVista;
    private final Reloj reloj;
    private final Thread hilo;
}
