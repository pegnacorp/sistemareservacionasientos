
package Gestores.cliente;

import java.io.IOException;
import modelo.*;

/**
 *
 * @author Arian Castillo
 * Descripción: Esta clase recibirá y enviará mensajes provenientes de un servidor
 * Dichos mensajes serán empaquetados/desempaquetados en la clase GestorJSON
 * Observar:
 * TCPCliente = para pruebas existirán muchos clientes en una computadora, pero eso no
 * sucede en el vida real, entonces se usarán más de 1 puertos
 */
public class GestorMensajeCliente {

        public GestorMensajeCliente() throws IOException {
            tcpCliente = new TCPCliente(ipServidor, puerto);
        }
        
        public GestorMensajeCliente (String ipServidor, int puertoServidor) throws IOException{
            tcpCliente = new TCPCliente(ipServidor, puertoServidor);
        }
        
        public void enviarMensaje(String mensaje){
            tcpCliente.enviarMensaje(mensaje);
        }
        
        public String recibirMensaje() throws IOException{
            String mensajeRecibido = tcpCliente.recibirMensaje();
            return mensajeRecibido;
        }
        
        private TCPCliente tcpCliente;
        private String ipServidor = "192.168.230.54";
        private int puerto = 5555;
}
