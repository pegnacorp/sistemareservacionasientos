
package Helpers.cliente;

import GUI.cliente.VPrincipal;
import Gestores.cliente.GestorMensajeCliente;
import Gestores.cliente.GestorPrerreserva;
import Gestores.cliente.GestorReloj;
import Gestores.cliente.GestorReserva;
import Gestores.cliente.GestorSesion;
import java.awt.Color;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import modelo.Asiento;
import modelo.Mensaje;

/**
 *
 * @author Arian Castillo
 */
public class HelperVPrincipal {
    
    public HelperVPrincipal(VPrincipal vista) {
        this.vista = vista;
        this.asientosSeleccionados = new ArrayList<>();
        this.numAsientosSeleccionados = 0;
        try {
            System.out.println("HELPER: Conectandose con el servidor");
            gestorMsj = new GestorMensajeCliente();
            System.out.println("HELPER: Conexion establecida con el servidor");
        } catch (IOException ex) {
            String nl = System.getProperty("line.separator");
            Object mensajeError = "Error al conectarse con el servidor." + 
                    nl + nl + "Detalles: " + nl + ex;
            JOptionPane.showMessageDialog(null, mensajeError, "ERROR DE CONEXION", JOptionPane.ERROR_MESSAGE);
            System.exit(0);
        }
        this.gestorClk = new GestorReloj(vista.getReloj());
        this.gestorPre = new GestorPrerreserva(gestorMsj);
        this.gestorRes = new GestorReserva(gestorMsj);
        this.gestorSes = new GestorSesion(gestorMsj);
        System.out.println("HELPER: Iniciando sesion");
        iniciarSesion();
    }
    
    public void asientoSelecionado(JButton boton){
        if(boton.getBackground() == Color.GREEN && numAsientosSeleccionados < 5){
            vista.getOkBtn().setEnabled(true);
            vista.getCancelBtn().setEnabled(true);
            ArrayList<JButton> botones = this.vista.getListaBotones();
            for(int i = 0; i < botones.size(); i++){
                if(boton.getName().equals(botones.get(i).getName())){
                    this.asiento = new Asiento(i+1,1);
                    break;
                }
            }
            msj = this.gestorPre.prerreservar(asiento);
            if(msj.getTipoMensaje() == 6 && msj.isOperacionRealizada()){
                this.asientosSeleccionados.add(asiento);
                numAsientosSeleccionados++;
                boton.setBackground(Color.BLACK);
                if(!this.gestorClk.estaIniciado())
                    this.gestorClk.iniciarReloj();
                else
                    this.gestorClk.reiniciarReloj();
            }
        }
    }
    
    private void iniciarSesion(){
        String ip = obtenerIP();
        System.out.println("HELPER: Esperando mensaje de inicio de sesion");
        msj = this.gestorSes.iniciar(ip,6618);
        System.out.println("HELPER: Mensaje recibido");
        System.out.println("HELPER: Tipo de mensaje = " + msj.getTipoMensaje());
        System.out.println("HELPER: Numero del 1er asiento en la lista = " + msj.getListaAsientos().get(0).getNumAsiento());
        if(msj.getTipoMensaje() == 6){
            System.out.println("HELPER: Sesion iniciada");
            pintarBotones(msj);
        }
    }
    
    public void cerrarSesion(){
        if(numAsientosSeleccionados>0)
            cancelar();
        this.gestorSes.cerrar();
        System.exit(0);
    }
    
    public void ok(){
        int index;
        for(index = 0; index < this.asientosSeleccionados.size(); index++){
            System.out.println("HELPER: Reservando el asiento " + (index+1));
            this.msj = this.gestorRes.reservar(this.asientosSeleccionados.get(index));
            if(this.msj.getTipoMensaje() == 6 && !this.msj.isOperacionRealizada()){
                break;
            }
        }
        if(this.msj.isOperacionRealizada()){
            this.gestorClk.reiniciarReloj();
            this.gestorClk.detenerReloj();
            this.vista.getOkBtn().setEnabled(false);
            vista.getCancelBtn().setEnabled(false);
            ArrayList<JButton> botones = vista.getListaBotones();
            for (int i = 0; i < botones.size(); i++) {
                if (botones.get(i).getBackground() == Color.BLACK) {
                    botones.get(i).setBackground(Color.ORANGE);
                }
            }
            Object mensajeCorrecto = "Asientos reservados.";
            JOptionPane.showMessageDialog(null, mensajeCorrecto, "OPERACION REALIZADA", JOptionPane.INFORMATION_MESSAGE);
        }
        else{
            Object mensajeError = "Error al reservar el asiento " + (index+1) + ".";
            JOptionPane.showMessageDialog(null, mensajeError, "ERROR AL RESERVAR", JOptionPane.ERROR_MESSAGE);
            for(int j = index; j > 0; j--){
                this.msj = this.gestorPre.cancelar(this.asientosSeleccionados.get(j-1));
                if(this.msj.getTipoMensaje() == 6 && !this.msj.isOperacionRealizada()){
                    mensajeError = "Error al cancelar los asientos que se prerreservaron.";
                    JOptionPane.showMessageDialog(null, mensajeError, "ERROR AL RESERVAR", JOptionPane.ERROR_MESSAGE);
                    break;
                }
            }
            ArrayList<JButton> botones = vista.getListaBotones();
            volverAsientosDisponibles(botones);
        }
        asientosSeleccionados.clear();
    }
    
    public void cancelar(){
        ArrayList<JButton> botones = vista.getListaBotones();
        for(int i = 0; i < this.asientosSeleccionados.size(); i++){
            this.msj = this.gestorPre.cancelar(this.asientosSeleccionados.get(i));
            if(this.msj.getTipoMensaje() == 6 && !this.msj.isOperacionRealizada()){
                break;
            }
        }
        this.vista.getOkBtn().setEnabled(false);
        vista.getCancelBtn().setEnabled(false);
        volverAsientosDisponibles(botones);
        this.asientosSeleccionados.clear();
        this.numAsientosSeleccionados = 0;
        this.gestorClk.reiniciarReloj();
        this.gestorClk.detenerReloj();
    }
    
    
    private ArrayList<Asiento> asientosSeleccionados;
    private Asiento asiento;
    private Mensaje msj;
    private int numAsientosSeleccionados;
    
    private GestorMensajeCliente gestorMsj;
    private GestorReloj gestorClk;
    private GestorPrerreserva gestorPre;
    private GestorReserva gestorRes;
    private GestorSesion gestorSes;
    
    private VPrincipal vista;
    
    private void volverAsientosDisponibles(ArrayList<JButton> botones){
        for(int i = 0; i<botones.size(); i++){
            if(botones.get(i).getBackground() == Color.BLACK)
                botones.get(i).setBackground(Color.GREEN);
        }
    }
    
    private void pintarBotones(Mensaje msj){
        System.out.println("HELPER: Pintando botones");
        ArrayList<Asiento> listaAsientos = msj.getListaAsientos();
        ArrayList<JButton> botones = vista.getListaBotones();
        for(int i = 0; i < botones.size(); i++){
            String estadoAsiento = listaAsientos.get(i).getEstado();
            if(botones.get(i).getBackground() != Color.BLACK){
                if(estadoAsiento.compareTo("libre")==0)
                    botones.get(i).setBackground(Color.GREEN);
                if(estadoAsiento.compareTo("reservado")==0)
                    botones.get(i).setBackground(Color.ORANGE);
                if(estadoAsiento.compareTo("prereservado")==0)
                    botones.get(i).setBackground(Color.BLUE);
            }
        }
        System.out.println("HELPER: Botones pintados");
    }
    
    private String obtenerIP(){
        System.out.println("HELPER: Obteniendo IP");
        String ip = JOptionPane.showInputDialog(null,"Introduce tu ip");
//        try {
////            ip = "192.168.230.113";//InetAddress.getLocalHost().getHostAddress();
//        ip = InetAddress.getLocalHost().getHostAddress();
//            System.out.println("HELPER: IP obtenida " + ip);
//        } catch (UnknownHostException ex) {
////            String nl = System.getProperty("line.separator");
////            Object mensajeError = "Error al crear buffer para obtener IP." + 
////                    nl + nl + "Detalles: " + nl + ex;
////            JOptionPane.showMessageDialog(null, mensajeError, "ERROR DE CONEXION", JOptionPane.ERROR_MESSAGE);
//        }
        return ip;
    }
}
