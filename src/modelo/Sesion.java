/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.ArrayList;

/**
 *
 * @author user
 */
public class Sesion {
    private String codigo;
    private ArrayList<Asiento> asientosReservados;
    
public Sesion(String codigo, ArrayList<Asiento> asientosReservados) {
    this.codigo = codigo;
    this.asientosReservados = asientosReservados;
}

    public String getCodigo() {
        return codigo;
    }

    public ArrayList<Asiento> getAsientosReservados() {
        return asientosReservados;
    }

    public void setAsientosReservados(ArrayList<Asiento> asientosReservados) {
        this.asientosReservados = asientosReservados;
    }
    
}
