
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.servidor;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Timer;
import java.util.TimerTask;

/**
 *
 * @author user
 */
public class GestorReloj {

    final Timer timer;
    private Calendar calendario;
    private boolean relojDetenido = false;
    private String palabra = "nada";

    public GestorReloj() {
        calendario = new GregorianCalendar();
        timer = new Timer();
        
    }
    public boolean estaDetenidoReloj(){
        return relojDetenido;
    }

    public void detenerReloj() {
        timer.cancel();
        timer.purge();
        relojDetenido = true;
        System.out.println("Detuviste reloj");
    }

    private long calcularTiempoDeEnvio(Date horaClienteConDate) {
        Date horaServidorConDate = calendario.getTime();
        long horaClienteConLong = horaClienteConDate.getTime();
        long horaServidorConLong = horaServidorConDate.getTime();
        long tiempoDeEnvio = horaServidorConLong - horaClienteConLong;
//el metodo getTime te devuelve en mili segundos para saberlo en mins debes hacer
        tiempoDeEnvio = tiempoDeEnvio / (1000 * 60);
        return tiempoDeEnvio;
    }
    //Revisar

    public void iniciarConteo(Date horaClienteConDate) throws InterruptedException{
        //Modificar

        long tiempoDeEnvio = calcularTiempoDeEnvio(horaClienteConDate); 
        contar60Segundos((int) tiempoDeEnvio);
        
    }
    private void contar60Segundos(int segundosTranscurridos) throws InterruptedException {
        int segundosAEsperar = 10 - segundosTranscurridos;

        TimerTask timerTask = new TimerTask() {
            int contadorRepeticion = 0;

            public void run() {

                if (contadorRepeticion == 0) {
                    System.out.println("Iniciando conteo");
                    
                    contadorRepeticion++;

                } else {
                    timer.cancel();
                    timer.purge();
                    System.out.println("Conteo ha terminado");
                }
            }
        };
        // Aquí se pone en marcha el contador cada segundo.


        // Dentro de 0 milisegundos avísame cada periodo de tiempo
        long milisegundosAESperar = convertirSegundosAMilisegundos(segundosAEsperar);
        timer.scheduleAtFixedRate(timerTask, 0, milisegundosAESperar);

//        calendario.setTime(darHoraDeServidor());
//        calendario.add(Calendar.SECOND, segundos);

    }

    private long convertirSegundosAMilisegundos(long segundos) {

        return segundos * 1000;
    }

    private Date darHoraDeServidor() {
        Date horaServidor = calendario.getTime();
        return horaServidor;
    }

    public String getPalabra() {
        return palabra;
    }

    public static void main(String[] args) throws InterruptedException {

    }

}
