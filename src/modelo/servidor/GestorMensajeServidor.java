/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.servidor;

import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import modelo.*;

/**
 *
 * @author user
 */
public class GestorMensajeServidor {

    private Socket cliente;
    private BufferedReader lectura;
    private PrintWriter escritura;

    public GestorMensajeServidor(Socket cliente) throws IOException {
        this.cliente = cliente;
        inicializarBuferMenjesEnviados();
        inicializarBuferMenjesRecibidos();
    }

    /**
     *
     * @param mensaje es el mensaje que se desea enviar Se empaca el mensaje
     * antes de enviarlo al servidor
     */
    public void enviarMensaje(Mensaje mensaje) {
        String mensajeEmpacado;
        mensajeEmpacado = empacarMensaje(mensaje);
        escritura.println(mensajeEmpacado);
    }

    public Mensaje recibirMensaje() throws IOException {
        String mensaje;
        mensaje = lectura.readLine();
        Mensaje mensajeDesempaquetado = desempacarMensaje(mensaje);
        return mensajeDesempaquetado;
    }

    private void inicializarBuferMenjesRecibidos() throws IOException {
        lectura = new BufferedReader(new InputStreamReader(cliente.getInputStream()));
    }

    private void inicializarBuferMenjesEnviados() throws IOException {
        escritura = new PrintWriter(cliente.getOutputStream(), true);
    }

    private String empacarMensaje(Mensaje mensajeSinEmpaquetar) {
        Gson gson = new Gson();
        String mensajeEmpacado = gson.toJson(mensajeSinEmpaquetar);
        return mensajeEmpacado;
    }
    private Mensaje desempacarMensaje(String mensajeEmpaquetado) {
        Gson gson = new Gson();
        Mensaje mensaje= gson.fromJson(mensajeEmpaquetado, Mensaje.class);
        return mensaje;
    }
}
