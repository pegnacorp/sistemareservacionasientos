/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.servidor;

import modelo.Asiento;
import modelo.servidor.AccesoDatosAsiento;

/**
 *
 * @author user
 */
public class GestorPrerreserva {

    private AccesoDatosAsiento accesoDatosAsiento;


    public GestorPrerreserva(AccesoDatosAsiento accesoDatosAsiento) {
        this.accesoDatosAsiento = accesoDatosAsiento;

    }
//Falta avisar si se pudo o no agregar asiento

    public boolean prerreservar(int numAsientoPrerreservar){
        boolean sePudo = false;
        Asiento asientoARevisar;
        asientoARevisar = accesoDatosAsiento.darAsientoPorNumAsiento(numAsientoPrerreservar);
        System.out.println("Prerreservar "+asientoARevisar.getNumAsiento());

        if (numAsientoPrerreservar == asientoARevisar.getNumAsiento()) {
            String estadoAsiento = asientoARevisar.getEstado();
            if (estadoAsiento.equals("libre")) {
                accesoDatosAsiento.cambiarEstadoPorNumAsiento(numAsientoPrerreservar, "prereservado");
                sePudo = true;
                System.out.println("Se pudo :)");
            } else {
                System.out.println("Nanais :(");
            }
        }
        return sePudo;
    }

    public boolean desprerreservar(int numAsientoADesprerreservar) {
        boolean sePudo = false;
        Asiento asientoARevisar;
        asientoARevisar = accesoDatosAsiento.darAsientoPorNumAsiento(numAsientoADesprerreservar);

        if (numAsientoADesprerreservar == asientoARevisar.getNumAsiento()) {
            String estadoAsiento = asientoARevisar.getEstado();
            if (estadoAsiento.equals("prereservado")) {
                accesoDatosAsiento.cambiarEstadoPorNumAsiento(numAsientoADesprerreservar, "libre");
                System.out.println("Se pudo :)");
                sePudo = true;
            } else {
                System.out.println("Nanais :(");
            }
        }
        return sePudo;
    }
}
