/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.servidor;

import Gestores.cliente.GestorJSON;
import Gestores.cliente.GestorMensajeCliente;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.Asiento;
import modelo.Cliente;
import modelo.Mensaje;

/**
 *
 * @author user
 */
public class Servidor implements Runnable {
    private ArrayList<Cliente> clientes;
    private Socket cliente;
    private ServerSocket servidorSocket;
    private ArrayList<GestorMensajeCliente> listaGestoresMsj;
    private AccesoDatosAsiento accesoDatosAsiento;
    private ActualizadorDeCliente actualizadorDeCliente;

    public Servidor() throws IOException {
        int puerto = 5555;
        servidorSocket = new ServerSocket(puerto);
        clientes = new ArrayList<>();
        listaGestoresMsj = new ArrayList<>();
        actualizadorDeCliente = new ActualizadorDeCliente(listaGestoresMsj);
        accesoDatosAsiento = new AccesoDatosAsiento(actualizadorDeCliente);
    }

    public void arrancar() throws IOException {
        new Thread(this, "Servidor").start();

    }

    public void esperarConexionUsuario() {
        try {
            System.out.println("Esperando conexion por parte del usuario");
            cliente = servidorSocket.accept();
            System.out.println("Se ha conectado un usuario");
        } catch (IOException ex) {
            System.out.println("El cliente no se pudo conectar");
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        }
        Mensaje mensajeRecibido = null;
        try {
            GestorMensajeServidor gestorMensaje = new GestorMensajeServidor(cliente);
            System.out.println("Esperando solicitud de inicio de sesion");
            mensajeRecibido = gestorMensaje.recibirMensaje();
            System.out.println("Solicitud recibida");
        } catch (IOException ex) {
            System.out.println("Problemas al recibir la solicitud de inicio de sesion del usuario");
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        }
        GestorMensajeCliente gestormsj = null;
        try {
            System.out.println("Estableciendo conexion con el servidor del usuario");
            gestormsj = new GestorMensajeCliente(mensajeRecibido.getIp(), mensajeRecibido.getPuerto());
            System.out.println("Conexion establecida");
        } catch (IOException ex) {
            System.out.println("Problemas al establecer conexion con el servidor del cliente");
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        }
        listaGestoresMsj.add(gestormsj);
        Cliente clienteEntidad = new Cliente(cliente,mensajeRecibido.getIp(),mensajeRecibido.getPuerto());
        clientes.add(clienteEntidad);
    }

    @Override
    public void run() {
        try {
            GestorMensajeServidor gestorMensaje = new GestorMensajeServidor(cliente);
            AdministradorMensajeControl administradorMensajeControl;
            boolean seDesconecto = false;
            int contador = 0;
            
            while (true) {
                if (contador == 0) {
                    ArrayList<Asiento> asientos = accesoDatosAsiento.darAsientos();
                    Mensaje mensaje = new Mensaje(6);
                    imprimirAsientos(asientos);
                    mensaje.setListaAsientos(asientos);
                    System.out.println("Devolviendo asientos para inicio de sesion");
                    gestorMensaje.enviarMensaje(mensaje);
                    System.out.println("Mensaje enviado");
                } else {
                    System.out.println("Recibiendo mensaje...");
                    Mensaje mensajeRecibido = gestorMensaje.recibirMensaje();
                    System.out.println("El mensaje recibido es de tipo " + mensajeRecibido.getTipoMensaje());
                    administradorMensajeControl = new AdministradorMensajeControl(cliente,clientes,accesoDatosAsiento);
                    System.out.println("Tomando medidas");
                    administradorMensajeControl.ejecutarAccionMensaje(mensajeRecibido);
                    boolean exitoso = administradorMensajeControl.darResultadoAccionMensaje();
                    System.out.println("Medidas tomadas");
                    System.out.println("Notificando a los clientes de los cambios");
                    actualizadorDeCliente.notificarClientes();
                    System.out.println("Notificacion exitosa");
                    System.out.println("Preparando mensaje de respondiendo al cliente");
                    Mensaje mensajeRespuesta = new Mensaje(6);
                    mensajeRespuesta.setOperacionRealizada(exitoso);
                    System.out.println("Enviando mensaje");
                    gestorMensaje.enviarMensaje(mensajeRespuesta);
                    System.out.println("Mensaje respondido");
                    if (seDesconecto == true) {
                        System.out.println("Usuario desconectado");
                        break;
                    }

                }
                contador++;
            }

        } catch (IOException ex) {
        } catch (InterruptedException ex) {
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void imprimirAsientos(ArrayList<Asiento> asientos) {
        for (int i = 0; i < asientos.size(); i++) {
            System.out.println(asientos.get(i).getNumAsiento() + " " + asientos.get(i).getEstado());
        }
    }
    public static void main(String[] args) throws IOException {
        String hostAddress = InetAddress.getLocalHost().getHostAddress();
        System.out.println("IP del servidor: " + hostAddress);
        Servidor servidor = new Servidor();
        while (true) {
            servidor.esperarConexionUsuario();
            servidor.arrancar();
        }

    }
}
