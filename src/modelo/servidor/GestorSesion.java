/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.servidor;

import modelo.Asiento;
import modelo.Sesion;

/**
 *
 * @author user
 */
public class GestorSesion {

public Sesion regresarSesion(String codigo){
    AccesoDatosSesion accesoDatosSesion = new AccesoDatosSesion();
    Sesion sesion = accesoDatosSesion.darSesionPorCodigo(codigo);
    return sesion;
}
    
}
