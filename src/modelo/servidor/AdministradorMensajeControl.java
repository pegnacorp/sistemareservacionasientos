/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.servidor;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.Cliente;
import modelo.Mensaje;
import modelo.Sesion;

/**
 *
 * @author user
 */
public class AdministradorMensajeControl {

    private Socket cliente;
    private AccesoDatosAsiento accesoDatosAsiento;
    private GestorReloj gestorReloj;
    private boolean resultadoAccionMensaje;
    private ArrayList<Cliente> clientes;
    ActualizadorDeCliente actualizadorDeCliente;

    public AdministradorMensajeControl(Socket cliente,ArrayList<Cliente> clientes,AccesoDatosAsiento accesoDatosAsiento) {
        this.cliente = cliente;
        this.accesoDatosAsiento = accesoDatosAsiento;
        gestorReloj = new GestorReloj();
        this.clientes = clientes;
    }

    public void ejecutarAccionMensaje(Mensaje mensaje) throws InterruptedException {
        //Modificar el númer del idAccion
        int idAccion = mensaje.getTipoMensaje();
        int numAsiento = 8; //Número que no está en las opciones

        switch (idAccion) {
            case 0:
                resultadoAccionMensaje = iniciarSesion(mensaje);
                break;
            case 1:
                cerrarConexion();
                break;
            case 2:
                resultadoAccionMensaje = prerreservar(mensaje);
                break;
            case 3:
                resultadoAccionMensaje = desprerreservar(mensaje);
                break;
            case 4:
                resultadoAccionMensaje = reservar(mensaje);
                break;
            case 5:
                resultadoAccionMensaje = desrreservar(mensaje);
                break;
            case 7:
                resultadoAccionMensaje = regresarSesion(mensaje);
                break;

        }
    }
    //Corregir el boleano que regresa

    public boolean regresarSesion(Mensaje mensaje) {
        System.out.println("Regresar sesión");
        resultadoAccionMensaje = false;
        System.out.println("Sesión");
        boolean sePudo = false;
        GestorSesion gestorSesion = new GestorSesion();
        Sesion sesion = gestorSesion.regresarSesion(mensaje.getCodigo());
        if (sesion == null) {
            sePudo = false;
        } else {
            GestorMensajeServidor gestorMensaje;
            try {
                gestorMensaje = new GestorMensajeServidor(cliente);
                Mensaje mensajeAEnviar = new Mensaje(6);
                mensajeAEnviar.setListaAsientos(sesion.getAsientosReservados());
                gestorMensaje.enviarMensaje(mensaje);
            } catch (IOException ex) {
            }
        }
        return sePudo;
    }

    public boolean darResultadoAccionMensaje() {
        return resultadoAccionMensaje;
    }

    private boolean iniciarSesion(Mensaje mensaje) {
        System.out.println("Iniciar sesión");
        resultadoAccionMensaje = false;
        return true;
//        GestorSesion gestorSesion = new GestorSesion();

    }

    private boolean prerreservar(Mensaje mensaje) throws InterruptedException {
        System.out.println("Prerreservar");
        resultadoAccionMensaje = false;
        System.out.println("Prerreservando");
        int numAsiento = mensaje.getAsiento().getNumAsiento();
        GestorPrerreserva gestorPrerreserva = new GestorPrerreserva(accesoDatosAsiento);
        boolean sePudo = gestorPrerreserva.prerreservar(numAsiento);


//        if(sePudo == true){
//            
//            //AGregar el date
//            gestorReloj.iniciarConteo(null);    
//          while (true) {
//            if (gestorReloj.estaDetenidoReloj() == true) {
//                gestorReloj.detenerReloj();
//            }
//        }
//        }
        return sePudo;
    }

    private boolean desprerreservar(Mensaje mensaje) {
        System.out.println("Desprerreservar");
        resultadoAccionMensaje = false;
        int numAsiento = mensaje.getAsiento().getNumAsiento();
        GestorPrerreserva gestorPrerreserva = new GestorPrerreserva(accesoDatosAsiento);
        //Falta conocer la estructura del mensaje

        boolean sePudo = gestorPrerreserva.desprerreservar(numAsiento);


        return sePudo;
    }

    private boolean reservar(Mensaje mensaje) {
        System.out.println("Reservar");
        resultadoAccionMensaje = false;
        int numAsiento = mensaje.getAsiento().getNumAsiento();
        GestorReserva gestorReserva = new GestorReserva(accesoDatosAsiento);
        boolean sePudo = gestorReserva.reservar(numAsiento);
        return sePudo;
    }

    private boolean desrreservar(Mensaje mensaje) {
        System.out.println("Desrreservar");
        resultadoAccionMensaje = false;
        int numAsiento = mensaje.getAsiento().getNumAsiento();
        GestorReserva gestorReserva = new GestorReserva(accesoDatosAsiento);
        boolean sePudo = gestorReserva.desrreservar(numAsiento);
        return sePudo;
    }

    private void cerrarConexion() {
        try {
            System.out.println("Se desconecto");
            cliente.close();
            clientes.remove(cliente);
        } catch (IOException ex) {
            System.out.println("Problemas al cerrar la conexión");
        }
    }
}
