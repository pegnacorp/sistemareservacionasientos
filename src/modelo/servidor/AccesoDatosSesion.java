/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.servidor;

import java.util.ArrayList;
import modelo.Asiento;
import modelo.Sesion;



/**
 *
 * @author user
 */
public class AccesoDatosSesion {
    ArrayList<Sesion> sesiones;
    
    public AccesoDatosSesion() {
        sesiones = new ArrayList<>();
    }
        public Sesion darSesionPorCodigo(String codigoRecibido,ActualizadorDeCliente actualizadorDeCliente) {
        Sesion sesionARevisar = null;
        for (int i = 0; i < sesiones.size(); i++) {//Inicio de for

            sesionARevisar = sesiones.get(i);
            if (codigoRecibido.equals(sesionARevisar.getCodigo())) {
                break;
            }
        }//Fin de for      
        return sesionARevisar;
    }

    public void cambiarAsientosPorCodigo(String codigoRecibido, ArrayList<Asiento> asientos) {
        for (int i = 0; i < sesiones.size(); i++) {//Inicio de for
            if (sesiones.get(i).getCodigo().equals(codigoRecibido)) {
                sesiones.get(i).setAsientosReservados(asientos);
            }
        }//Fin de for      
    }
    public void crearSesion(Sesion sesion){
        boolean sesionNueva = true;
        for(int i = 0; i<sesiones.size();i++){
        if(sesiones.get(i).getCodigo().equals(sesion.getCodigo())){
            sesionNueva =false;
            break;
        }
        if(sesionNueva == true){
            sesiones.add(sesion);
        }
        
        }
    }

    Sesion darSesionPorCodigo(String codigo) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
