/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.servidor;

import java.util.ArrayList;
import modelo.Asiento;

/**
 *
 * @author user
 */
public class AccesoDatosAsiento {

    ArrayList<Asiento> asientos;
    ActualizadorDeCliente actualizadorDeCliente;

    public AccesoDatosAsiento(ActualizadorDeCliente actualizadorDeCliente) {
        this.actualizadorDeCliente = actualizadorDeCliente;
        asientos = new ArrayList<>();
        Asiento asiento1 = new Asiento(1, 0);
        asientos.add(asiento1);
        Asiento asiento2 = new Asiento(2, 0);
        asientos.add(asiento2);
        Asiento asiento3 = new Asiento(3, 0);
        asientos.add(asiento3);
        Asiento asiento4 = new Asiento(4, 0);
        asientos.add(asiento4);
        Asiento asiento5 = new Asiento(5, 0);
        asientos.add(asiento5);
        Asiento asiento6 = new Asiento(6, 0);
        asientos.add(asiento6);
        Asiento asiento7 = new Asiento(7, 0);
        asientos.add(asiento7);
        Asiento asiento8 = new Asiento(8, 0);
        asientos.add(asiento8);
        Asiento asiento9 = new Asiento(9, 0);
        asientos.add(asiento9);
        Asiento asiento10 = new Asiento(10, 0);
        asientos.add(asiento10);
        Asiento asiento11 = new Asiento(11, 0);
        asientos.add(asiento11);
        Asiento asiento12 = new Asiento(12, 0);
        asientos.add(asiento12);
        Asiento asiento13 = new Asiento(13, 0);
        asientos.add(asiento13);
        Asiento asiento14 = new Asiento(14, 0);
        asientos.add(asiento14);
        Asiento asiento15 = new Asiento(15, 0);
        asientos.add(asiento15);
        Asiento asiento16 = new Asiento(16, 0);
        asientos.add(asiento16);
        Asiento asiento17 = new Asiento(17, 0);
        asientos.add(asiento17);
        Asiento asiento18 = new Asiento(18, 0);
        asientos.add(asiento18);
        Asiento asiento19 = new Asiento(19, 0);
        asientos.add(asiento19);
        Asiento asiento20 = new Asiento(20, 0);
        asientos.add(asiento20);
        Asiento asiento21 = new Asiento(21, 0);
        asientos.add(asiento21);
        Asiento asiento22 = new Asiento(22, 0);
        asientos.add(asiento22);
        Asiento asiento23 = new Asiento(23, 0);
        asientos.add(asiento23);
        Asiento asiento24 = new Asiento(24, 0);
        asientos.add(asiento24);
        Asiento asiento25 = new Asiento(25, 0);
        asientos.add(asiento25);
    }

    public ArrayList<Asiento> darAsientos() {
        return asientos;
    }

    public Asiento darAsientoPorNumAsiento(int numAsientoADesprerreservar) {
        Asiento asientoARevisar = null;
        for (int i = 0; i < asientos.size(); i++) {//Inicio de for

            asientoARevisar = asientos.get(i);
            if (numAsientoADesprerreservar == asientoARevisar.getNumAsiento()) {
                break;
            }
        }//Fin de for      
        return asientoARevisar;
    }

    public void cambiarEstadoPorNumAsiento(int numAsiento, String estado) {
        for (int i = 0; i < asientos.size(); i++) {//Inicio de for
            if (asientos.get(i).getNumAsiento() == numAsiento) {
                asientos.get(i).setEstado(estado);
                actualizadorDeCliente.cambiarEstado(asientos);
            }
        }//Fin de for      
        imprimirAsientos();
    }

    private void imprimirAsientos() {
        for (int i = 0; i < asientos.size(); i++) {
            System.out.println(asientos.get(i).getNumAsiento() + " " + asientos.get(i).getEstado());
        }
    }
}
