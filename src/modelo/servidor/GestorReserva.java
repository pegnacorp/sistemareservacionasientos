/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.servidor;

import modelo.Asiento;

/**
 *
 * @author Juan Carlos Peña Moreno Nota: no olvida construir el controlador
 */
public class GestorReserva {

    AccesoDatosAsiento accesoDatosAsiento;

    public GestorReserva(AccesoDatosAsiento accesoDatosAsiento) {
        this.accesoDatosAsiento = accesoDatosAsiento;
    }

    public boolean desrreservar(int numAsientoPrerreservar) {
        boolean sePudo = false;
        //falta contenido
        return sePudo;
    }

    public boolean reservar(int numAsientoPrerreservar) {
        boolean sePudo = false;
        Asiento asientoARevisar;
        asientoARevisar = accesoDatosAsiento.darAsientoPorNumAsiento(numAsientoPrerreservar);

        if (numAsientoPrerreservar == asientoARevisar.getNumAsiento()) {
            String estadoAsiento = asientoARevisar.getEstado();
            if (estadoAsiento.equals("prereservado")) {
                accesoDatosAsiento.cambiarEstadoPorNumAsiento(numAsientoPrerreservar, "reservado");
                System.out.println("Se pudo :)");
                sePudo = true;
            } else {
                System.out.println("Nanais :(");
            }
        }
        return sePudo;
    }


}
