
package modelo;

/**
 *
 * @author Arian Castillo
 */
public class Asiento {
    private int numAsiento;
    private String estado;

    public Asiento(int numAsiento, int estado) {
        this.numAsiento = numAsiento;
        switch(estado){
            case 0:
                this.estado = LIBRE;
                break;
            case 1:
                this.estado = PRERESERVADO;
                break;
            case 2:
                this.estado = RESERVADO;
                break;
            default:
                System.out.println("ERROR AL GENERAR EL ASIENTO");
                break;
        }
    }
    
    public int getNumAsiento() {
        return numAsiento;
    }

    public void setNumAsiento(int numAsiento) {
        this.numAsiento = numAsiento;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    
    private static final String LIBRE = "libre";                //Valor en int: 0
    private static final String PRERESERVADO = "prereservado";  //Valor en int: 1
    private static final String RESERVADO = "reservado";        //Valor en int: 2
}
