
package modelo;

import java.util.ArrayList;

/**
 *
 * @author Arian Castillo
 */
public class Mensaje {
    
    public Mensaje(int tipoMensaje){
        switch(tipoMensaje){
            case 0:
                this.tipoMensaje = INICIAR_SESION;
                break;
            case 1:
                this.tipoMensaje = CERRAR_SESION;
                break;
            case 2:
                this.tipoMensaje = PRERRESERVA;
                break;
            case 3:
                this.tipoMensaje = CANCELAR_PRERRESERVA;
                break;
            case 4:
                this.tipoMensaje = RESERVA;
                break;
            case 5:
                this.tipoMensaje = CANCELAR_RESERVA;
                break;
            case 6:
                this.tipoMensaje = RESPUESTA_SERVIDOR;
                break;
            case 7:
                this.tipoMensaje = REGRESAR_SESION;
                break;
            default:
                System.out.println("ERROR AL GENERAR EL MENSAJE");
                break;
        }
    }

    public boolean isOperacionRealizada() {
        return operacionRealizada;
    }

    public void setOperacionRealizada(boolean operacionRealizada) {
        this.operacionRealizada = operacionRealizada;
    }
    
    public int getTipoMensaje() {
        return tipoMensaje;
    }

    public void setTipoMensaje(int tipoMensaje) {
        this.tipoMensaje = tipoMensaje;
    }

    public Asiento getAsiento() {
        return asiento;
    }

    public void setAsiento(Asiento asiento) {
        this.asiento = asiento;
    }

    public ArrayList<Asiento> getListaAsientos() {
        return listaAsientos;
    }

    public void setListaAsientos(ArrayList<Asiento> listaAsientos) {
        this.listaAsientos = listaAsientos;
    }
    
    public void setCodigo(String codigo){
        this.codigo = codigo;
    }
    public String getCodigo(){
        return codigo;
    }
        public void setIp(String ip){
        this.ip = ip;
    }
    public String getIp(){
        return ip;
    }
    public void setPuerto(int puerto){
        this.puerto = puerto;
    }
    public int getPuerto(){
        return puerto;
    }
    
    private String ip;
    private int puerto;
    private boolean operacionRealizada;
    private int tipoMensaje;
    private Asiento asiento;
    private ArrayList<Asiento> listaAsientos;
    private String codigo;
    //Constantes de sesion
    private static final int INICIAR_SESION = 0;
    private static final int CERRAR_SESION = 1;
    //Constantes para prerreserva
    private static final int PRERRESERVA = 2;
    private static final int CANCELAR_PRERRESERVA = 3;
    //Constantes para reserva
    private static final int RESERVA = 4;
    private static final int CANCELAR_RESERVA = 5;
    //Constantes para responder si el servidor realizo sus operaciones
    private static final int RESPUESTA_SERVIDOR = 6;
    private static final int REGRESAR_SESION = 7;
    
}
