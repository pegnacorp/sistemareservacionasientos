/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.net.Socket;

/**
 *
 * @author user
 */
public class Cliente{
private Socket socket;
private String ip;
private int puerto;

    public Cliente(Socket socket, String ip, int puerto) {
        this.socket = socket;
        this.ip = ip;
        this.puerto = puerto;
    }

    public Socket getSocket() {
        return socket;
    }

    public void setSocket(Socket socket) {
        this.socket = socket;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getPuerto() {
        return puerto;
    }

    public void setPuerto(int puerto) {
        this.puerto = puerto;
    }




    
}
