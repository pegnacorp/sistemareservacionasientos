/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 *
 * @author user
 */
public class TCPServidor {

    private Socket cliente;
    private BufferedReader lectura;
    private PrintWriter escritura;
    private ServerSocket servidorSocket;

    public TCPServidor(int puerto) throws IOException {
        servidorSocket = new ServerSocket(puerto);

    }

    public void esperarConectarseCliente() throws IOException {
        cliente = servidorSocket.accept();//Este método es bloqueante
        inicializarBuferMenjesEnviados();
        inicializarBuferMenjesRecibidos();
    }

    public void enviarMensaje(String mensaje) {
        escritura.println(mensaje);
    }

    public String recibirMensaje() throws IOException {
        String mensaje;
        mensaje = lectura.readLine();
        return mensaje;
    }

    private void inicializarBuferMenjesRecibidos() throws IOException {
        lectura = new BufferedReader(new InputStreamReader(cliente.getInputStream()));
    }

    private void inicializarBuferMenjesEnviados() throws IOException {
        escritura = new PrintWriter(cliente.getOutputStream(), true);
    }
    public void cerrarConexion() throws IOException{
        servidorSocket.close();
    }
    public int darPuerto(){
        return servidorSocket.getLocalPort();
        
    }
    public String darIp() throws UnknownHostException{
        return servidorSocket.getInetAddress().getLocalHost().getHostAddress();
    }
    

}
