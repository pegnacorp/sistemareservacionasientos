/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 *
 * @author Juan Carlos Peña
 */
public class TCPCliente {

    private Socket socket;

    private BufferedReader lectura;
    private PrintWriter escritura;

    public TCPCliente(String direccionServidor, int puertoServidor) throws IOException {
        socket = new Socket(direccionServidor, puertoServidor);
        socket.setSendBufferSize(10000);
        inicializarBuferMenjesRecibidos();
        inicializarBuferMenjesEnviados();
    }

    public void enviarMensaje(String mensaje) {
        escritura.println(mensaje);
    }

    public String recibirMensaje() throws IOException {
        String mensaje;
        mensaje = lectura.readLine();
        return mensaje;
    }

    private void inicializarBuferMenjesRecibidos() throws IOException {
        lectura = new BufferedReader(new InputStreamReader(socket.getInputStream()));
    }

    private void inicializarBuferMenjesEnviados() throws IOException {
        escritura = new PrintWriter(socket.getOutputStream(), true);

    }

    public void cerrarConexion() throws IOException {
        socket.close();
    }

    public int darPuertoServidor() throws IOException {
        return socket.getPort();
    }

}
