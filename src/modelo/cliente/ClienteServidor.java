
package modelo.cliente;

import Gestores.cliente.GestorCambios;
import java.awt.Color;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import modelo.Asiento;
import modelo.Mensaje;

/**
 *
 * @author Arian Castillo
 */
public class ClienteServidor implements Runnable {
    
    public ClienteServidor(ArrayList<JButton> listaBotones){
        try {
            int puerto = 6600;
            servidorSocket = new ServerSocket(puerto);
            this.listaBotones = listaBotones;
        } catch (IOException ex) {
            Logger.getLogger(ClienteServidor.class.getName()).log(Level.SEVERE, null, ex);
        }
        conexionEstablecida = false;
        hilo = new Thread(this);
        hilo.start();
    }
    
    public void terminar(){
        hilo.stop();
    }
    
    @Override
    public void run() {
        while(true){
            if(!conexionEstablecida){
                try {
                    System.out.println("SERVIDOR: Esperando conexion del servidor");
                    cliente = servidorSocket.accept();
                    System.out.println("SERVIDOR: Conexion por parte del servidor aceptada");
                } catch (IOException ex) {
                    String nl = System.getProperty("line.separator");
                    Object mensajeError = "Error al conectarse el servidor con el cliente." + 
                            nl + nl + "Detalles: " + nl + ex;
                    JOptionPane.showMessageDialog(null, mensajeError, "ERROR DE CONEXION", JOptionPane.ERROR_MESSAGE);
                }
            }
            conexionEstablecida = true;
            try {
                gestorCam = new GestorCambios(cliente);
                System.out.println("SERVIDOR: Esperando mensaje");
                msj = gestorCam.recibirMensaje();
                System.out.println("SERVIDOR: Mensaje recibido");
            } catch (IOException ex) {
                String nl = System.getProperty("line.separator");
                Object mensajeError = "Error al recibir informacion del servidor." + 
                        nl + nl + "Detalles: " + nl + ex;
                JOptionPane.showMessageDialog(null, mensajeError, "ERROR DE CONEXION", JOptionPane.ERROR_MESSAGE);
            }
            if(msj.getTipoMensaje() == 6){
                System.out.println("SERVIDOR: Se recibio los cambios de otros clientes");
                pintarBotones(msj);
            }
        }
    }
    
    private Socket cliente;
    private ServerSocket servidorSocket;
    private ArrayList<JButton> listaBotones;
    private GestorCambios gestorCam;
    private Mensaje msj;
    private boolean conexionEstablecida;
    private final Thread hilo;
    private void pintarBotones(Mensaje msj){
        ArrayList<Asiento> listaAsientos = msj.getListaAsientos();
        System.out.println("SERVIDOR: Cambiando color de los botones...");
        for(int i = 0; i < listaBotones.size(); i++){
            String estadoAsiento = listaAsientos.get(i).getEstado();
            if(listaBotones.get(i).getBackground() != Color.BLACK){
                if(estadoAsiento.compareTo("libre")==0)
                    listaBotones.get(i).setBackground(Color.GREEN);
                if(estadoAsiento.compareTo("reservado")==0)
                    listaBotones.get(i).setBackground(Color.ORANGE);
                if(estadoAsiento.compareTo("prereservado")==0)
                    listaBotones.get(i).setBackground(Color.BLUE);
            }
        }
    }
}
