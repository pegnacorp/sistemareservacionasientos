
package modelo;

import static java.lang.Thread.sleep;

/**
 *
 * @author Arian Castillo
 */
public class Reloj {
    private int segundos;
    private boolean iniciado = false;
    
    public void setSegundos(int segundos){
        this.segundos = segundos;
    }
    public int getSegundos(){
        return segundos;
    }
    
    public void setIniciado(boolean inicio){
        this.iniciado = inicio;
    }
    public boolean getIniciado(){
        return iniciado;
    }
}
